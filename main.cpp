
#include <iostream>
//     0    1      4             5
enum { ok, error, value = 4, nextvalue };

int main() {
  std::cout << ok << " " << error << " " << value << " " << nextvalue
            << std::endl;
}